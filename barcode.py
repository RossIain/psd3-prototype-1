import csv

class BarcodeReader:
	#List of barcode ids with associated students
	

	#Reads csv file and provides the list of students recorded				  
	def readFile(self, csvFileName, barcodeStudents):
		ids = [];
		with open(csvFileName, 'rb') as csvFileName:
			reader = csv.reader(csvFileName)
			for row in reader:
				ids += [barcodeStudents[row[0]]]
				#rint row[0]
		return ids



