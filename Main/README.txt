PROTOTYPE 1 TEAM F

This project relies on standard python libraries
and runs on python 2.7, python 3 not supported yet.

This program has only been tested on the x64 linux machines
in the lab, it may not have the same behaviour in another
environment.

Ensure when enetering data that the case is the same
as shown here

=====Data to Use=====

User Logins: 
	admin account: 
                  user "admin" password "1"

        tutor account:
		  user: "1103627i"  password: "4526"

        student account:
                  user: "1106566b", password: "1234"
 
                   
Student Ids:
	"1103627i", "1106566b"

CourseIDs: 
	"COMPSCI1001" COMPSCI1002"

Lab Weeks: 
	1,2,3

Session/Lab Letters: 
	A,B

Barcode upload file to use: "BarcodeData.txt"


====Running the program====

run program from main directory with "python2 main.py"

====Program Walkthrough====
.When starting the program you should be presented with
a login, enter one of the login details above.

You should now be brought to the main menu with a
list of options, depending on the account you 
logged in on the access to each of these options
will be restricted. An Admin can both
upload attendance and export csv. A Tutor can
only upload attendance. A student cannot do anything.
If you try to access an option which the account
doesn't have the access for you will be presented with
an error message.

 ====CSV Export====

  ====Specific Course====
  You should be asked for the course Id to export to a CSV file.
  Enter one of the Course Ids at the top of this document.

  Then you should be asked the name/path to give the file.
  For the simplest result just enter for example "test.csv"
  
  Once sucessful the program should return a "done" message.
  If you check the same directory you should find the test.csv
  file which should have all the details for the specific course
  in the required CSV format.


  ====Single Student====
  You should be asked for the student Id to export to a CSV file.
  Enter one of the Student Ids at the top of this document.

  Then you should be asked the name/path to give the file.
  For the simplest result just enter for example "test2.csv"
  
  Once sucessful the program should return a "done" message.
  If you check the same directory you should find the test2.csv
  file which should have all the details for the specific student
  in the required CSV format.

If you now press 'x' to return to the main menu as specified by
the program output on the terminal.

====Add Attendance=====
You should be asked for a course id, enter one of the course ids
from the top of this document.

You should then be asked for the session "letter"
this is the identifier for each seperate session in a week
,enter one of the letters from the top of this document (ensure
it's in uppercase e.g. A not a)

You should then be asked for the week number,
enter one of the weeks from the top of this document.

You should then be brought to a menu to select to 
upload a barcode scanner file containing the student details
or to manually enter the attendance details for the session.

 ====Manual Entering==== 
 You should be presented with a list of students for the session
 along with their current attendance status (present, absent, or mv)
 The default is absent until changed.

 Then enter one of the studen numbers from the list that
 you wish to change the status of, then enter either
 "mv", "present", or "absent".

 The details should be updated and the student list printed
 again and the same options show up. Repeat this until
 you wish to go back to the attendance menu by pressing x.

 ====Barcode Upload====
 Enter the barcode file name from the top of this document.

 The file should be read and
 all the students the barcode data maps to in the current session's
 status should be set to present.
 
 You are presented with the manual entering options incase
 any manual changes are needed. Press x when done.

Finally press x and then q to exit the program. 
 


