from datetime import *
import getpass
import os


class Course:
    def __init__(self, coursename, courseId, sessions, students):
        self.name = coursename 
        self.c = courseId
        self.sessions = sessions
        self.students = students
    def __repr__(self):
        return self.c
    def __str__(self):
        printstring = ""
        for session in self.sessions:
            printstring += str(session) + "\n"
        return str(self.c) + " has had the following sessions:\n" + printstring

    def getStudents(self):
        return self.students

    def getSessions(self):
        return self.sessions

    def getId(self):
        return self.c

    def getName(self):
        return self.name
    
class Student:
    def __init__(self, firstname, surname, iD):
        self.first = firstname
        self.last = surname
        self.iD = iD
       
    def __repr__(self):
        return self.iD
    def __str__(self):
        return self.iD + " : " + self.first + " " + self.last + "\n"
    
    def check(self, courses):
        printstream = "\n"
        for course in courses:
            printstream += str(self.iD) + " is enrolled in " + course.c + "\n"
            for s in course.sessions:
                for lab in s.labs:
                    if self.iD in lab.attendance:
                        printstream += "In the lab of week " +str(lab.week) + " " + str(self.iD) + " was " + str(lab.attendance[self.iD])+"\n"
        print (printstream)

    def getId(self):
        return self.iD

    def getFirstName(self):
        return self.first

    def getLastName(self):
        return self.last

class Session:
    def __init__(self, letter, begintime, endtime, labs):
        self.letter = letter
        self.et = endtime
        self.bt = begintime
        self.labs = labs
    def __repr__(self):
        return self.letter
    def __str__(self):
        return "The session " + self.letter + " goes from " + str(self.bt) + " to " + str(self.et)

    def getLabs(self):
        return self.labs

    def getLetter(self):
        return self.letter

class Lab:
    def __init__(self, week, attendance):
        self.week = week
        self.attendance = attendance
    def __repr__(self):
        return self.week
    def __str__(self):
        return "The atttendance of this lab is" + self.attendance

    def getAttendance(self):
        return self.attendance
        
    def update(self,idsList):
        
        for ids in idsList:
            if ids in self.attendance:
                self.attendance[ids]="present"
            


    def getWeek(self):
        return self.week


