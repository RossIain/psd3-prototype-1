from barcode import BarcodeReader
from csvWrapper import CSVCourseAttendance
from csvWrapper import CSVStudent
from classes import *

# data...

student1 = Student("Dayana","Ivanova","1103627i")
student2 = Student("Teodora","Buzea","1106566b")
students = [student1, student2]

course1 = Course("JP2", "COMPSCI1001",[Session("A",time(9,0,0),time(10,0,0),[Lab(1,{"1103627i":"mv","1106566b":"absent"}),
                                                                      Lab(2,{"1103627i":"present","1106566b":"absent"}),
                                                                      Lab(3,{"1103627i":"present","1106566b":"absent"})]),
                                Session("B",time(11,0,0),time(12,0,0),[Lab(1,{"2103627i":"mv","2106566b":"absent"}),
                                                                      Lab(2,{"2103627i":"present","2106566b":"absent"}),
                                                                      Lab(3,{"2103627i":"present","2106566b":"absent"})])],
                 students)

course2 = Course("Mastering Oragami 3", "COMPSCI1002",[Session("A",time(9,0,0),time(10,0,0),[Lab(1,{"1103627i":"mv","1106566b":"absent"}),
                                                                      Lab(2,{"2103627i":"present","1106566b":"absent"}),
                                                                      Lab(3,{"1103627i":"present","1106566b":"absent"})]),
                                Session("B",time(11,0,0),time(12,0,0),[Lab(1,{"2103627i":"mv","2106566b":"absent"}),
                                                                      Lab(2,{"1103627i":"present","2106566b":"absent"}),
                                                                      Lab(3,{"2103627i":"present","2106566b":"absent"})])],
                 students)

courses = [course1, course2]

lab1 = Lab(1,{"1103627i":"absent","1106566b":"absent","1105673s":"absent"})
lab2 = Lab(1,{"1103628i":"mv","1106566b":"absent"})
idsList = ["1103627i", "1105673s", "1106566b"]
lab1.update(idsList)

users = {"admin":{"password":"1", "access":"admin"}, "1106566b":{"password":"1234", "access":"student"},"1103627i": {"password":"4567", "access":"tutor"},"1105673s":{"password":"2345", "access":"student"}}
barcodeStudents= {'12345678536014': "1108765s", 
                                  '23456789457015': "1103627i",
                                  '34567890467016': "5436542w",
                                  '45678901400017': "6654763q",
                                  '56789901645017': "1106566b",}

#############################
#############################
#############################

### login method

def login(users, user):
        iD = user
        if iD in users: 
                passwd = raw_input("Type in your password: ")
                if(passwd == users[iD]["password"]):
                        print("Login successful!")
                        return True
                else:
                        print("Wrong password")
                        return False
        else:
                print("Matriculation number not found!")
                return False

### helper methods
def getStudentFromId(students, id):
    for student in students:
        if student.getId() == id:
            return student
    return []


def getCourse(id, courses):

    for course in courses:
        if id == course.getId():
            return course
    return None




def exportCourse():
        weeks = [1, 2, 3]

        courseId = raw_input("Enter course Id to download: ")

        outputCourse = getCourse(courseId, courses)
        if outputCourse == None:
            print("Error course "+courseId+" doesn't exist")
            return

        #Fill out details for each student in the course
        CourseAttendance = []
        for student in outputCourse.getStudents():
            labs = ["none", "none", "none"] #Default allocation none
            CurrentAttendance = [student.getFirstName(), student.getLastName(), 
            student.getId()]
            for session in outputCourse.getSessions():

                for lab in session.getLabs():
                    if student.getId() in lab.getAttendance():
                        letter = session.getLetter()
                        if lab.getWeek() in weeks:
                            labs[lab.getWeek()-1] = letter

            CourseAttendance += [CurrentAttendance + labs]

        print CourseAttendance

        #Export to file
        try:
                courseExport  = CSVCourseAttendance(CourseAttendance)
                fileName = raw_input("Enter file name to create: ")
                courseExport.export(fileName)
                print("done")
        except IOError:
                print("Error processing file")


def exportStudent():
        studentId = raw_input("Enter Student Id to download: ")

        validId = False
        for student in students:
            if student.getId() == studentId:
                validId = True
                break

        if not validId:
            print("Error student id not found")
            return

        studentDetails = []
        for course in courses:
            if getStudentFromId(course.getStudents(), studentId) != []: #check student takes course
                currentDetails = []
                for session in course.getSessions():
                    for lab in session.getLabs():
                        if studentId in lab.getAttendance():
                            currentDetails = []
                            currentDetails += [course.getName(), course.getId(),"Lab "+ str(lab.getWeek()),lab.getAttendance()[studentId]]
                            studentDetails += [currentDetails]            

        print studentDetails

        try:
                studentExport = CSVStudent(studentDetails)
                fileName = raw_input("Enter file name to create: ")
                studentExport.export(fileName)
                print("done")

        except IOError:
                  print("Error processing file")


def printAttendance(lab) :
    for id in lab.getAttendance():
        print ("student id: "+id+" attendance: "+lab.getAttendance()[id])

def CSVExport():
        csvOptions = "\nCSV Export\n"\
                     "----------\n"\
                     "'c' to export details from specific course\n"\
                     "'s' to export details for a single student\n"\
                     "'x' to return to main menu \n"\

        option = 'a'
        while(option != 'x'):
                print(csvOptions)                                  
                option = raw_input("Enter option: ")

                if option == 'c':
                        exportCourse()
                elif option == 's':
                        exportStudent()
                elif option != 'x':
                        print("invalid option: "+option)


#upload attendance from barcode CSV
def uploadCSV(lab):
        fileName = raw_input("Enter filename to upload: ")
        try:
                barcodes = BarcodeReader()
                attendance  = barcodes.readFile(fileName, barcodeStudents) #list of iDs
         
                lab.update(attendance)
                manual(lab) #Allow a manual check after automatic csv upload
                                
        except IOError:
                print("Error processing file")


#Manually add attendance for a session
def manual(lab):
    
    print "Manual check of  attendance:"
    attendances = ["mv", "present", "absent"]
    while True:
        print("\n")
        printAttendance(lab)
        studentNo = raw_input("Enter student number, or x to exit: ")
        if studentNo == 'x':
            return
        elif studentNo not in lab.getAttendance():
            print "Error, student "+studentNo+" isn't assigned to this session"
            continue

        attendance = raw_input("Enter attendance to change to (mv, present, absent): ")
        if attendance == 'x':
            return
        elif attendance in attendances:
            lab.getAttendance()[studentNo] = attendance
        else:
            print 'Error, must enter either "mv", "present", or "absent"'


def addAttendance():
        session = None
        course = None
        courseId = raw_input("Enter course id: ")
        course = getCourse(courseId, courses)

        if(course == None):
            print "Error, course doesn't exist"
            return

        sessions = course.getSessions()
        sessionId = raw_input("Enter session letter: ")
        for s in sessions:
            if s.getLetter() == sessionId:
                session = s
                break;

        if(session == None):
            print "Error, session doesn't exist"
            return

        weekNo = raw_input("Enter week number: ")
        if not weekNo.isdigit():
            print "Error, enter a digit for the week number"
            return

        lab = None
        for l in session.getLabs():
            if l.getWeek() == int(weekNo):
                lab = l
                break

        if lab == None:
            print "Error, lab "+weekNo+" does not exist for this session"
            return

        attendanceOptions = "\nAdd Attendance\n"\
                                           "----------\n"\
                                           "'u' to upload barcode scanner file\n"\
                                           "'m' to manually enter attendance details\n"\
                                           "'x' to return to main menu \n"\

        option = 'a'
        while(option != 'x'):
                print(attendanceOptions)                                   
                option = raw_input("Enter option: ")


                if option == 'u':
                        uploadCSV(l)
                elif option == 'm':
                        manual(l)
                elif option != 'x':
                        print("invalid option: "+option)
        


#############################
#############################
#############################

# MAIN METHOD
user = raw_input("Type in your matriculation number: ")
if login(users, user):
        accType = users[user]["access"]
        menuDisplay = "\nPrototype Main Menu \n"\
                                  "------------------- \n"\
                                  "'c' to export CSV   \n"\
                                  "'a' to add attendance\n"\
                                  "'v' to view courses\n"\
                                  "'q' to quit program \n"\

        option = 's'
        while(option != 'q'):
                print(menuDisplay)
                option = raw_input("Enter option: ")

                if(option == 'c'):
                        if accType == "admin": #Only admins can export CSV
                                CSVExport()
                        else:
                                print("You do not have access rights to perform this option")

                elif(option == 'a'):
                        if accType == "admin" or accType == "tutor":
                                addAttendance()  #only tutors or admins can add attendance
                        else:
                                print("You do not have access rights to perform this option")
                elif(option == 'v'):
                        matric = user
                        if accType != "student": #Students cannot do anything yet
                                matric = raw_input("Enter the matriculation number of the student:")
                        for course in courses:
                                for student in course.students:
                                        if student.iD == matric:
                                                student.check([course])

                elif(option != 'q'):
                        print("invalid option: "+option)



                


