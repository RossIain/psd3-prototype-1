#Parent class for exporting CSV files
import csv

class CSVExport(object):
	
	#Constructor
	def __init__(self, headers, data): 
		self.headers = headers #list of Strings containing CSV headers
		self.data = data #list of Tuples contaning CSV rows of data

	#Add a CSV row 
	def addRow(self, data):
		self.data.append(data)

	#Export CSV to specified directory
	def export(self, fileName):
		with open(fileName, 'wb') as csvfile:
			csvwriter = csv.writer(csvfile, delimiter=',',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			csvwriter.writerow(self.headers) #write header row

			for row in self.data:	#write data rows
				csvwriter.writerow(row)

#Export all details on student attendance for course
class CSVCourseAttendance(CSVExport):
	
	headers = ["FirstName","Surname","ID Number","Assignment: Laboratory 1",
		   "Assignment: Laboratory 2", "Assignment: Laboratory 3"]

	def __init__(self, studentSessions):
		super(CSVCourseAttendance, self).__init__(self.headers, studentSessions) #Instansiate Superclass	


#Export all student details
class CSVStudent(CSVExport):
	headers =["Course","ID number","Assignment","Mark"]

	def __init__(self, assignments):
		super(CSVStudent, self).__init__(self.headers, assignments) #Instansiate Superclass	
		



